#!/bin/bash

Usage () {
  echo "wget_WFN.sh: a script for retrieving WFN files for the N10 BerkeleyGW benchmark."
  echo "Usage: wget_WFN.sh <size>"
  echo "Allowed sizes:"
  echo "[ small     (   3 GB )," 
  echo "  medium    (  18 GB ),"
  echo "  reference (  71 GB ),"
  echo "  target    ( 216 GB ) ]"
}

NERSC_TEN=https://portal.nersc.gov/project/m888/nersc10
Si_WFN_url=$NERSC_TEN/benchmark_data/BGW_input

case "$1" in

  --help)
  Usage
  exit
  ;;

  small)
    WFN_gz=Si214_WFN_file.tar.gz
    ;;

  medium)
    WFN_gz=Si510_WFN_file.tar.gz
    ;;

  reference)
    WFN_gz=Si998_WFN_file.tar.gz
    ;;

  target)
    WFN_gz=Si2742_WFN_file.tar.gz
    ;;

  *)
    echo "Error: the requested WFN size ($1) is not supported.)"
    Usage
    exit 1

esac

wget $Si_WFN_url/$WFN_gz
tar -xvf $WFN_gz
