# NERSC-10 BerkeleyGW Workflow Input WFN Files

This directory contains the input WFN files for the BerkeleyGW workflow benchmarks.
The lattices are very large in size and are not kept in the repository.
You can find the lattice files at

`https://portal.nersc.gov/project/m888/nersc10/benchmark_data/BGW_input`

You can download them to your local machine using `wget`
The `wget_WFN.sh` script is provided to simplify the download process.

If you are on a NERSC platform, you can copy them from the following directory

`/global/cfs/cdirs/m888/www/nersc10/benchmark_data/BGW_input`

Note that on NERSC's Lustre filesystems, 
I/O performance is greatly improved if the destination directory is "striped" before copying.

```
$ #<you should be in the Si_WFN_folder directory>
$ stripe_large .
$ BGW_input=/global/cfs/cdirs/m888/www/nersc10/BGW_input
$ for file in $BGW_input/*; do cp $file .; done
```