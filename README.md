This repository describes the Optical Properties of Materials workflow component benchmark
from the [NERSC-10 Benchmark Suite](https://www.nersc.gov/systems/nersc-10/benchmarks).<br>
The [NERSC-10 benchmark run rules](https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/N10_Benchmark_RunRules.pdf)
should be reviewed before running this benchmark.<br>
Note, in particular:
- The NERSC-10 run rules apply to the Optical Properties of Materials workflow component benchmark
  except where explicitly noted within this README.
- The run rules define "baseline", "ported" and "optimized" categories of performance optimization.
- Responses to the NERSC-10 RFP should include performance estimates for the "baseline" category;
  results for the "ported" and "optimized" categories are optional.
- The Optical Properties of Materials benchmark defines multiple problem sizes.
RFP responses should estimate the performance for the target problem on the target system.
- NERSC has defined a reference time by running the reference problem on Perlmutter. 
  For the ported and optimized categories,
  the projected walltime for the target problem on the target system must not exceed the reference time.
  Concurrency adjustments (i.e. weak- or strong-scaling) may be needed to match the reference time.
  **The constraint on the projected walltime does not apply to the baseline category.**
- The "capability factor" (c) describes the increase in
computational work (e.g. flops) between the reference and target problems,
and may be used to guide resource requirments for the target problem.
The capability factor is also used  to compute  the
[Sustained System Improvement (SSI) metric]( https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/Workflow_SSI.pdf).


# 0. Workflow Overview

Predicting optical properties of materials and nanostructures
is a key step toward developing future energy conversion materials and electronic devices.
The BerkeleyGW code is widely used for this type of simulation workflow.
A [typical workflow](http://manual.berkeleygw.org/3.0/overview-workflow/)
takes some [mean field](http://manual.berkeleygw.org/3.0/meanfield/)-related quantities 
from DFT-based codes such as PARATEC, Abinit, PARSEC, Quantum ESPRESSO, OCTOPUS and SIESTA.
Then BerkeleyGW's Epsilon module computes the material's dielectric function.
The Sigma module uses the output of the preceding steps to compute the electronic self energy.
Two other modules, Kernel and Absorption,
can build upon the output from Epsilon and Sigma
to calculate the electron-hole interactions and neutral optical excitation properties.

<img width="500" alt="BGW overview scheme" src="figures/overview_scheme.png" title="BGW overview scheme" >

This benchmark focuses on the Epsilon and Sigma stages of the workflow;
the DFT, Kernel and Absorbtion stages are not included in this benchmark.
There are multiple approaches to computing the matrix elements of the self-energy operator;
for this benchmark, the General Plasmon Pole (GPP) approximation is used.

The BerkeleyGW code is mostly written primarily in Fortran, with some C and C++,
and containins about 100,000 lines of code,
It is parallelized using MPI and OpenMP on the CPU, and OpenACC/OpenMP-target constructs on GPUs.
The project website is https://berkeleygw.org, and its documentation is available from 
http://manual.berkeleygw.org/3.0/.
A paper derscribing the details of its implementation is published here: 
https://arxiv.org/abs/1111.4429.
BerkeleyGW is distributed under the Berkeley Software Distribution (BSD) license.
Please see the [license.txt](BerkeleyGW/license.txt) and 
[Copyright.txt](BerkeleyGW/Copyright.txt) files 
for more details.

## 0.1 Epsilon 

The Epsilon module for the GPP approach has three main computational kernels:
* MTXEL: Matrix elements computation
* CHI-0: Static Polarizability
* Inversion: Matrix inversion of the static polarizability (LU decomposition + triangular inversion)

For the series of input problems distributed with this benchmark,
the computational complexity of Epsilon
increases quartically,$`O(N^4)`$, with the number of atoms.


## 0.2 Sigma  

The Sigma module for the GPP approach has two main computational kernels:
* MTXEL: Matrix element computation
* GPP: Computing the quasiparticle energies for a set of electronic states

For the series of input problems distributed with this benchmark,
the computational complexity of Sigma
increases quartically, $`O(N^4)`$, with the number of atoms.

## 0.3 Parallel decomposition

Both Epsilon and Sigma use a two-tier MPI decomposition to exploit the available  parallelism:
* Inter-Pool: MPI ranks are divided into pools, with each pool of ranks working on a subset of quasiparticle energies (QP) independently
* Intra-Pool: within each pool, all the MPI ranks work together to calculate a subset of quasiparticle energies.

# 1. BerkeleyGW Code Access and Compilation Details

The instructions below can be used to build BerkeleyGW
for the GPU-accelerated nodes of NERSC's Perlmutter system
(AMD EPYC + NVIDIA Ampere).
This example is not intended to prescribe how to build BerkeleyGW;
some modifications may be needed to build BerkeleyGW
for other target architectures.

## 1.0 Build Environment

Before beginning, it is convenent to store the path to directory
that contains this README.md  file in the N10_BGW variable:
```
N10_BGW=$(pwd)
```

BerkeleyGW depends on multiple external software packages,
and tested extensively with various configurations.

| Category | Dependency<br>Level | Tested Packages |
|---       |---                  |---                 |
| Operating system | required   | Linux, AIX, MacOS  |
| Fortran compiler | required   | pgf90, ifort, gfortran, g95, openf90, sunf90, pathf90,<br>crayftn, af90 (Absoft), nagfor, xlf90 (experimental) |
| C compiler       | required   | pgcc, icc,  gcc, opencc, pathcc, craycc, clang   |
| C++ compilers    | required   | pgCC, icpc, g++, openCC, pathCC, crayCC, clang++ |
| FFT              | required   | FFTW versions 3.3.x |
| LAPACK/BLAS      | required   | NetLib, ATLAS, Intel MKL, ACML, Cray LibSci      |
| MPI              | optional   | OpenMPI, MPICH1, MPICH2, MVAPICH2, Intel MPI     |
| ScaLAPACK/BLACS  | optional<br>(required if MPI is used) |  NetLib, Cray LibSci, Intel MKL, AMD |

On Perlmutter, these libraries can be loaded by module commands:
```bash
module swap PrgEnv-gnu PrgEnv-nvhpc
module load cray-hdf5-parallel
module load cray-fftw
module load cray-libsci
module load python 
```

## 1.1 Downloading BerkeleyGW

The results reported [below](#32-timing) were obtained using 
a pre-release snapshot of the main branch of the BerkeleyGW code, 
containing the latest features to run on GPU systems.
All baseline and ported results should use the provided snapshot.
These features will be publicly available in the next major 4.0 release.

* Enter the `N10_BGW directory, then download and unzip  BerkeleyGW source code:
```bash
cd $N10_BGW
BGW_source_url=https://portal.nersc.gov/project/m888/nersc10/benchmark_data/BGW_input/
wget $BGW_source_url/BerkeleyGW-master_pm.zip
unzip BerkeleyGW-master_pm.zip
```

 **Update (July 27, 2023):** 
 A development version of the BerkeleyGW source code has been added to `$BGW_source_url/BerkeleyGW-master_frontier.zip`.
This version adds modifications and configuration files to support AMD GPUs and may be used for optimized (not baseline) performance.
This _develepment_ version is shared _as-is_, despite known bugs 
(e.g. the Epsilon module cannot use more than 256 MPI processes).
Results should be checked carefully for correctness.

## 1.2 Configuring BerkeleyGW

The BerkeleyGW build system is based on `make`
and requires manual configuration by editing an architecture-specific makefile named `arch.mk`.
Example `arch.mk` files for various supercomputers
are provided in the `$N10_BGW/BerkeleyGW-master/config` directory.

* Select the file most closely related to the target environment and copy it. For example:
```bash
cd $N10_BGW/BerkeleyGW-master
cp config/perlmutter.nersc.gov-nvhpc-openacc.mk arch.mk
```

* Edit `arch.mk` to fit your needs,for example, by adding the appropriate library paths.
Refer to the [BerkeleyGW manual](http://manual.berkeleygw.org/3.0/compilation-flags/) for more options.
       
## 1.3 Compiling BerkeleyGW
Stay in the `BerkeleyGW-master` directory to compile the various BerkeleyGW modules. 
(Many modules will be compiled, but this benchmark suite only uses Epsilon and Sigma.)
The following  command will generate the complex (`cplx`) version of the code.
```
make -j cplx 
```
After compilation, the excutables (`epsilon.cplx.x` and `sigma.cplx.x`)
will be in the source directory.
Symbolic links with the same name will be in the `BerkeleyGW-master/bin` directory.

# 2. Running the BerkeleyGW benchmark

The [`$N10_BGW/benchmark`](/benchmark) directory contains four problem sizes:
| Problem<br>Size | Atoms | Capability<br>Factor |
|---              |---                |---   |
| small           | Si<sub> 214</sub> |  2.1x10<sup>-3</sup>|
| medium          | Si<sub> 510</sub> |  6.8x10<sup>-2</sup>|
| reference       | Si<sub> 998</sub> |  1.0 |
| target          | Si<sub>2742</sub> | 57.0 |

Each problem simulates a silicon divacancy defect embedded in a series of progressively larger supercells.
The small, medium and reference problems are provided to facilitate testing and profiling.
The target problem is the intended benchmark.
SSI reference values from NERSC's Perlmutter system were evaluated using the reference problem size.

## 2.1 Download wave-function data

Each problem requires several data files
that must be downloaded prior to running the benchmarks.
The largest of these are the `.WFN` (wave-function) files.
These files are large and are provided separately to avoid accidental download.
The workflow for each problem size can be executed using only the corresponding data files.
(To run the medium workflow, only the medium files need be downloaded.)

The data files should be downloaded to the `Si_WFN_folder` directory.
Note that it may be possible to reduce I/O time
by moving the `Si_WFN_folder` to a high performance filesystem prior to the download,
and distributing the directory over multiple disks ("striping").
A description of striping on Perlmutter's Lustre filesystem is
[here](https://docs.nersc.gov/performance/io/lustre/).
Explicit striping instructions are not provided here because
the commands and optimal settings are not transferable to other filesystems.

The files are available from 
the [NERSC BerkeleyGW Benchmark data portal](https://portal.nersc.gov/project/m888/nersc10/benchmark_data/BGW_input)
and can be retrived using `wget`.
The `wget_WFN.sh` script is provided to simplify the download process:
```
$ cd Si_WFN_folder
$ ./wget_WFN.sh --help
| Usage: wget_WFN.sh <size>
| Allowed sizes: 
|  [ small     (   3 GB ), 
|    medium    (  18 GB ), 
|    reference (  71 GB ), 
|    target    ( 216 GB ) ]
```

## 2.2 Update site-specific files
Enter `$N10_BGW/benchmark` folder
and edit the `site_path_config.sh` script
to specify the location of required libraries,
BerkeleyGW executable (`bin/`) folder
and folders with large I/O files. In particular:
* `HDF_LIBPATH=` path to location of libraries, if any.
* `BGW_DIR=` path to epsilon.cplx.x and sigma.cplx.x (i.e., the `BerkeleyGW-master/bin` directory created in the previous section).
* `Si_WFN_folder=` path to large I/O downloaded files (`$Si_WFN_folder/` from previous section).


## 2.3 Submit

Each problem size has its own subdirectory within `$N10_BGW/benchmark`.
Each of those directories contains the input files needed for Epsilon and Sigma,
and a submit script suitable for NERSC's Perlmutter system.
For example, to run the medium size Epsilon calculation on Perlmutter, 
after having appropriately modified `berkeleygw-workflow/benchmark/site_path_config.sh`, do:
```
cd $N10_BGW/benchmark/medium_Si510/
sbatch run_epsilon_Si510.sh 
```

Each Perlmutter GPU node has 4 NVIDIA A100 GPUs and 1 AMD Milan CPU.
The parallel configuration for all runs on Perlmutter used 4 MPI tasks per node, 
and each MPI task uses 1 GPU and 16 CPU cores. 
To run on systems different than Permutter, 
modify the run scripts to reflect the hardware specifics of the architecture of interest.
For Epsilon, there are no constraints on the number of MPI tasks that may be used.
For Sigma, the number of MPI tasks must be divisible by `number_sigma_pools` set in `sigma.inp`.
The input files (`epsion.inp` and `sigma.inp`) may not be modified **except**:
- to optimize the MPI decomposition using `number_sigma_pools`.
- to optimize the maximum GPU memory per MPI rank (in GB) to use for Epsilon's chi summaton phase using the `max_mem_nv_block_algo` flag in `epsion.inp`. This flag can have a strong influence on time to solution: more memory typically improves performance. Half the device memory is a reasonable initial guess.

The `run_epsilon_*` scripts will generate 
the `BGW_EPSILON_$SLURM_JOBID` folder where the calculations will run,
and all output files will be written to this directory.
The `$SLURM_JOB_ID` variable will be defined by SLURM when the job is submitted.
The main results, including timing information, are directed to standard output,
which will be directed to `BGW_EPSILON_$SLURM_JOBID.out`
The `run_sigma_*` scripts are organized similarly, 
but with filenames that substitute `SIGMA` in place of `EPSILON`

# 3. Results

The run scripts in the benchmark directories will write standard output 
to `BGW_EPSILON_$SLURM_JOBID.out` or `BGW_SIGMA_$SLURM_JOBID.out`,
with `$SLURM_JOBID` being the job id assigned to your job by Slurm at submission.
These stdout files contain the information needed 
to determine the programs' correctness and performance.
In addition, the `eqp1.dat` file is needed to verify the correctness of Sigma;
it can be found in the same directory as the stdout files.


## 3.1 Correctness & Timing

Correctness can be verified using the `benchmark/BGW_validate.sh` script,
which compares values from the output to their expected output.
The result of the validation test is printed
on the first line of the script output. For example:
```
$ ../BGW_validate.sh: test output correctness for the NERSC-10 BerkeleyGW benchmark.
|  Usage: BGW_validate.sh <app> <size> <output_file>
|  Allowed apps: [ epsilon, sigma ]
|  Allowed sizes: [ small, medium, reference, target ]
|  Example: BGW_validate.sh epsilon small BGW_EPSILON.out

$ ../BGW_validate.sh: epsilon small BGW_EPSILON.out
|  Testing epsilon small
|  Validation:    PASSED
|  Total Time:     62.45
|  I/O Time:        5.33
|  Benchmark Time: 57.12
```
In addition, these scripts will print several performance results for the job.
Total Time corresponds to the full duration of the executed job.
I/O Time is the time spent writing data to disk.
Benchmark Time is computed by subtracting the I/O times from the Total Time.
Only the Benchmark Time will be used for the evaluation of BerkeleyGW benchmark results.

## 3.2 Performance on Perlmutter

The sample data in the table below are measured runtimes from NERSC's Perlmutter GPU system.
Perlmutter's  GPU nodes have one AMD Milan CPU and four NVIDIA 40GB A100 GPUs.
Each job used four MPI tasks per node, each with one GPU and 16 cores.
The upper rows of the table describe the weak-scaling performance of BerkeleyGW.
Lower rows describe the strong-scaling performance when runningn the reference problem.
The reference times are marked by a *.

| Problem<br>Size       | Nodes<br>Used | | Epsilon<br>Total Time<br>(Seconds)  | Epsilon<br>I/O Time<br>(Seconds) | Epsilon<br>Benchmark Time<br>(Seconds)    | | Sigma<br>Runtime<br>(Seconds)| Sigma<br>I/O Time<br>(Seconds) | Sigma<br>Benchmark Time<br>(Seconds) |
|---         |---   |-|---      |---    |---     |-|---      |---    |---      |
| small      |    1 | |  195.46 | 13.32 | 182.14 | |  534.89 |  2.97 |  531.92 |
| medium     |   16 | |  279.35 |  7.48 | 271.87 | |  871.23 |  3.76 |  867.47 |
| medium     |   32 | |  152.99 |  5.04 | 147.95 | |  543.97 |  4.42 |  539.55 |
| reference  |   64 | | 1123.69 | 27.19 |1096.50 | | 3095.66 |  7.30 | 3088.36 |
| reference  |  128 | |  559.16 | 16.22 | 542.94 | | 1566.87 | 13.36 | 1553.51 |
| reference  |  256 | |  354.55 | 10.23 | 344.32*| |  784.25 |  8.69 |  775.56*|
| reference  |  512 | |  214.03 |  6.02 | 208.01 | |  405.57 | 10.69 |  394.88 |
| reference  | 1024 | |  186.75 |  7.19 | 179.56 | |  241.30 | 16.12 |  225.18 |


## 3.4 Reporting

Benchmark results should include projections of the Benchmark Time
for the target problem size (Si-2742) for both stages (reported individually) 
of the BerkeleyGW workflow. 
The hardware configuration (i.e. the number of elements from each pool of computational resources) 
needed to achieve the estimated timings must also be provided. 
For example, if the target system includes more than one type of compute node, 
then report the number and type of nodes used to run each stage of the workflow. 
If the target system enables disaggregation/ composability, 
a finer grained resource list is needed, 
as described in the [Workflow-SSI document]( https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/Workflow_SSI.pdf ).

For the electronic submission, 
include all the source and makefiles used to build on the target platform 
and input files and runscripts. 
Include all standard output files.
